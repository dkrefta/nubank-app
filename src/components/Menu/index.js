import React from "react"
import QRCode from 'react-native-qrcode-svg';
import { Container, Code, Nav, NavItems, NavText, SignOutButton, SignOutButtonText} from "./styles"
import Icon from "react-native-vector-icons/MaterialIcons"


export default function Menu({ translateY }) {
    return (
        <Container style={{
            opacity: translateY.interpolate({
              inputRange: [0, 150],
              outputRange: [0, 1],
            }),
          }}>
            <Code>
            <QRCode 
            value="https://www.nubank.com.br"
            size={80}
            bgColor="#FFF"
            fgColor="#8B10AE"
            />
            </Code>
        <Nav>
            <NavItems>
            <Icon  name="help-outline" size={20} color="#FFF"/>
            <NavText> Me Ajuda </NavText>
            </NavItems>
            <NavItems>
            <Icon  name="person-outline" size={20} color="#FFF"/>
            <NavText> Perfil </NavText>

            </NavItems>
            <NavItems>
            <Icon  name="credit-card" size={20} color="#FFF"/>
            <NavText> Configurar Cartão </NavText>

            </NavItems>
            <NavItems>
            <Icon  name="smartphone" size={20} color="#FFF"/>
            <NavText>  Configurações</NavText>
            </NavItems>
        </Nav>
        <SignOutButton onPress={() => {}}> 
        <SignOutButtonText> Sair do App </SignOutButtonText>
        </SignOutButton>
        </Container>

    )
}

